$fa=1;
$fs=0.5;

difference() {
    cylinder(h=3, d=120, center=true);
    cylinder(h=5, d=60, center=true);
    cube([15, 80, 5], center=true);
};
