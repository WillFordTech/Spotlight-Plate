# Spotlight Plate

This project is a very simple OpenSCAD model to create a support plate for a spotlight with worn springs so that it will sit more flush to the ceiling.

The model is intended to be exported for 3D printing.